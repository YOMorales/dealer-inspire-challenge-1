# Dealer Inspire Code Challenge, Answer By YOMorales

This is an answer to the Dealer Inspire PHP Code Challenge. This answer was done
by me, Yamir Ortiz-Morales (YOMorales).

## Overview

This answer to the challenge consists of a micro app that processes data from an HTML contact form
and sends the data to an email address and stores it in a database.

## Running This App

These are the steps for preparing the app:

1) Clone the app:

```
git clone git@bitbucket.org:YOMorales/dealer-inspire-challenge-1.git
cd dealer-inspire-challenge-1
```

2) Change permissions of `storage` directory:

```
chmod -R 775 storage/
```

3) Download required packages with composer:

```
composer install
```

4) Change these database settings in `.env` file to allow access to your database server:

```
DB_HOST
DB_PORT
DB_USERNAME
DB_PASSWORD
```

An existing `.env` file is already provided. (I understand that it is bad practice to check an .env file to
the repo, but I did it anyway to make faster the process of reviewing this app and ask less from you).

5) Create the app's database. The following command will create the database specified in `.env` file.

```
php artisan db:create
```

## Evaluating the App

These are the steps for using and reviewing the app:

1) Run the test cases. From the root `dealer-inspire-challenge-1` directory, run:

```
phpunit
```

The test suite will use Lumen's migration files to setup the necessary table. It will also create
code coverage reports which can be seen in the directory `/tests/reports/coverage`. I love to have
those graphs colored green!

2) Use the app via a browser. First, you will need to run the db migration files again, as the
testsuite tore down the table when it finished testing.

```
php artisan migrate
```

3) Then run php's built-in server:

```
php -S 127.0.0.1:9999 -t public
```

And type the following address in your browser address bar: `127.0.0.1:9999`

You will see the contact form. I added some JS validations to its fields. Also, when you submit it, the
form will be sent via ajax to the backend server and confirmation message will be output.
During the processing of the form's data, validation/sanitation will be done, and the data will be
saved to the database and sent in an email.
I'm using Laravel's 'Log Driver' for mailing, so you will see the email's result in the log
file `/storage/logs/lumen.log`

## Final Notes

* Regarding framework choice, Phalcon would have been my first option, but using it would
have required that you install Phalcon's PHP extension in your local machine. Or I could have
managed it by creating a Docker container. But this would have been too much work for the
purpose of this app.

	* So I decided to go with Laravel, but then it would have too much dependencies that I didn't
want to include out of the box, like sessions, cache, etc.

	* I opted to use Lumen in the end, because it was slimmer for this micro app.
However, down the road I found that recent versions of Lumen (5+) trimmed the amount of features
that it had, and I had to perform several 'hacks' for it, taking much more time to develop with
Lumen. I 'hacked' it to be able to use things like Laravel's mailing system, custom artisans commands, etc.
But in the end, it was cool to do everything I did.

* I had to adjust to a couple of style changes in Lumen, like using 4 spaces instead of tabs.
Normally I use tabs because of this: https://softwareengineering.stackexchange.com/a/72
Also, had to force myself to put the opening braces of classes and functions in a new line.

* I always joke that I personally have the `--verbose` flag on. I love to be verbose in my explanations,
my code comments, my commit messages, and my code. That is why you will see long variable names. I do not
tolerate ambiguous variables like `$ab`. I'm a coder that knows that code is read more often than written,
hence I take pains to make it more readable even if I have to type more.

* As for db migrations: I wanted to do a full programmatic way to setup the database and went with
Laravel's migration system. However, one limitation is that it does not support creation of db, so I did an
artisan command for that. See this link, which guided me in doing it:
https://anthonysterling.com/posts/using-an-artisan-command-to-create-a-database-in-laravel-or-lumen.html
I know this was overkill, since I could have simply provided you with an .sql file. But I wanted to have
the opportunity to use Artisan and migrations in this micro app.

* I would have done a different approach to database testing, since Lumen's one was too simple and limited.
I would have used PHPUnit's database testing features (DBUnit), and would have set a different database for
testing, fixtures, and other methods of PHPUnit for that. But that would have required much more work for
this test project. I still can do it if you want.

* Other bonus tasks that I would have done (maybe I will do them anyway):

	* Save the contact form data to two other databases besides MySQL (perhaps PostgreSQL and MongoDB).

	* Use Eloquent models instead.
