<?php
/**
 * Custom artisan command for creating the necessary database.
 *
 * The artisan commands provided by Lumen do not allow the creation of a database (although they
 * allow db migrations). So, this custom command is provided as an automatic way of creating
 * the needed database.
 *
 * PHP version 7.1
 *
 * @package MessagesApp\Console
 * @category Console
 * @version 1.0.0
 *
 * @filesource
 *
 * @author Y.O.Morales <yomorales@gmail.com>
 * @copyright 2018 Dealer Inspire
 * @license http://dealerinspire.com Proprietary software.
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use \PDO;
use \PDOException;

/**
 * DatabaseCreate
 *
 * @since 1.0.0
 */
class DatabaseCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string $signature
     * @since 1.0.0
     */
    protected $signature = 'db:create';

    /**
     * The console command description.
     *
     * @var string $description
     * @since 1.0.0
     */
    protected $description = 'Creates a new database using the connection, name, and charset defined in .env file.';

    /**
     * Create a new command instance.
     *
     * @return void
     * @since 1.0.0
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * See inline comments for more info.
     *
     * @return void
     * @throws \PDOException If creating the database gave any kind of PDO error.
     * @see DatabaseCreate::getPDOConnection()
     * @since 1.0.0
     */
    public function handle()
    {
        // not setting defaults because values for these settings must be in one place only (.env) to avoid duplication
        $database = env('DB_DATABASE', false);
        $charset = env('DB_CHARSET', false);
        $collation = env('DB_COLLATION', false);

        // ensures that the required environment variables are set
        if ($database == false) {
            $this->error('Please, define DB_DATABASE in the .env file.');
            return;
        }

        if ($charset == false) {
            $this->error('Please, define DB_CHARSET in the .env file.');
            return;
        }

        if ($collation == false) {
            $this->error('Please, define DB_COLLATION in the .env file.');
            return;
        }

        // attempts to create the database
        try {
            // 'manually' connects to db server
            $pdo = $this->getPDOConnection(env('DB_HOST'), env('DB_PORT'), env('DB_USERNAME'), env('DB_PASSWORD'));

            /*
            Creates the database.
            Didn't use parameter binding as the values are not gotten from user input, but also because without
            specifying a db in getPDOConnection, $statement->execute() couldnt execute.
            */
            $result = $pdo->exec(sprintf(
                'CREATE DATABASE IF NOT EXISTS %s DEFAULT CHARACTER SET %s COLLATE %s;',
                $database,
                $charset,
                $collation
            ));

            // handles errors, results, etc
            if ($result != true) {
                throw new PDOException('Query did not return true after execution.');
            }

            $this->info(sprintf('Successfully created the %s database.', $database));
        } catch (PDOException $exception) {
            $this->error(sprintf('Failed to create the %s database. Error: %s', $database, $exception->getMessage()));
        }
    }

    /**
     * Connects to the database server.
     *
     * @param string $host The hostname of the database server.
     * @param integer $port The port of the database server.
     * @param string $username The database server user to use in the connection.
     * @param string $password The password for the user to connect to the server.
     * @return object an instance of the PDO object.
     * @since 1.0.0
     */
    private function getPDOConnection($host, $port, $username, $password)
    {
        return new PDO(sprintf('mysql:host=%s;port=%d;', $host, $port), $username, $password);
    }

}
