<?php
/**
 * Controller tasked with handling data from contact form.
 *
 * PHP version 7.1
 *
 * @package MessagesApp\Controllers
 * @category Controllers
 * @version 1.0.0
 *
 * @filesource
 *
 * @author Y.O.Morales <yomorales@gmail.com>
 * @copyright 2018 Dealer Inspire
 * @license http://dealerinspire.com Proprietary software.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactFormMail;

/**
 * MessagesController
 *
 * @since 1.0.0
 */
class MessagesController extends Controller
{

    /**
     * A list of fields that should be expected from the contact form.
     *
     * @var array $whitelist_fields
     * @since 1.0.0
     */
    public $whitelist_fields = ['full_name', 'email', 'phone', 'guest_message'];

    /**
     * Create a new controller instance.
     *
     * @return void
     * @since 1.0.0
     */
    public function __construct()
    {
        //
    }

    /**
     * Processes the data coming from the contact form to our backend system.
     *
     * See inline comments for more info.
     *
     * @param Request An instance of the Request class.
     * @return string A message indicating success of this method.
     * @see MessagesController::retainWhitelistedFields()
     * @see MessagesController::sanitizeInput()
     * @see ContactFormMail
     * @since 1.0.0
     */
    public function add(Request $request)
    {
        // validates using Lumen's validation system
        // see: https://lumen.laravel.com/docs/5.5/validation
        $validatedData = $this->validate($request, [
            'Message.full_name' => 'bail|required|string|min:6|max:80',
            'Message.email' => 'bail|required|email|max:80',
            'Message.phone' => 'bail|nullable|regex:/^[0-9\-\(\) ]+$/|max:20',
            'Message.guest_message' => 'required|string|min:10|max:510'
        ]);

        // gets data from contact form's request
        // note: used 'Message' topmost array element as a way to group fields
        $data = $request->input()['Message'];

        // eliminates any other field that might have been injected in the form, leaving only the ones we want
        $data = $this->retainWhitelistedFields($data);

        // sanitizes
        $data = $this->sanitizeInput($data);

        // saves to database; we already stripped unwanted fields above, and we now use bound parameters
        app('db')->insert('INSERT INTO messages (full_name, email, phone, message) VALUES (:full_name, :email, :phone, :guest_message)', $data);

        // also sends the data in an email
        Mail::to(env('MAIL_TO_ADDRESS'))->send(new ContactFormMail($data));

        return '{"status":"success"}';
    }

    /**
     * Takes an array of fields and eliminates any field not in a whitelist.
     *
     * @param array $all_fields All fields passed from a request or any other input.
     * @return array The same array passed as parameter, but without fields that are not present in a whitelist.
     * @see MessagesController::$whitelist_fields
     * @since 1.0.0
     * @TODO: move this to a separate class later, perhaps a model or middleware
     */
    public function retainWhitelistedFields(array $all_fields)
    {
        // we need to convert $whitelist_fields to keys in order for array_intersect_key below to work
        $whitelist_fields = array_fill_keys($this->whitelist_fields, null);
        $cleaned_fields = array_intersect_key($all_fields, $whitelist_fields);
        // ensures that the whitelisted fields are always present, otherwise MySQL will complain about bound params
        return array_merge($whitelist_fields, $cleaned_fields);
    }

    /**
     * Does some sanitation to each element in array param, plus other transformations.
     *
     * In a real app, security would be improved through other means like enforcing a stricter validation of the
     * full_name field, or escaping the data like for example using htmlspecialchars() if the data is to be
     * rendered in an HTML page (although Blade does this), among other recommendations (see OWASP site).
     * Also, when saving the data to the db, bound parameters are used (see add() method above).
     * But achieving even more degree of security is not within the scope of this test app.
     *
     * See inline comments for more info.
     *
     * @param array $data The input data to sanitize, each array element containing a field or column.
     * @return array The same array passed as parameter, but after some transformations.
     * @since 1.0.0
     * @TODO: move this to a separate class later, perhaps a model or middleware
     */
    public function sanitizeInput(array $data)
    {
        array_walk($data, function(&$value, $key) {
            // we want to make sure that any character that will be stripped below is not missed due to url encoding
            $value = urldecode($value);
            // this will strip tags, encode quotes, among other things
            $value = filter_var($value, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_BACKTICK);
            // trims spaces that might have been left from sanitation above
            $value = trim($value);
            // converts empty strings to null (Laravel has a middleware for that, but Lumen doesn't)
            $value = empty($value) ? null : $value;
        });

        return $data;
    }

}
