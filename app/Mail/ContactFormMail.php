<?php
/**
 * Class that will prepare emails containing the contact form data.
 *
 * PHP version 7.1
 *
 * @package MessagesApp\Mail
 * @category Mail
 * @version 1.0.0
 *
 * @filesource
 *
 * @author Y.O.Morales <yomorales@gmail.com>
 * @copyright 2018 Dealer Inspire
 * @license http://dealerinspire.com Proprietary software.
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * ContactFormMail
 *
 * @since 1.0.0
 */
class ContactFormMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The data from the contact form, which will be emailed.
     *
     * Made protected as per the Laravel documentation.
     *
     * @var array $data
     * @since 1.0.0
     */
    protected $data;

    /**
     * Create a new message instance and populates the $data property.
     *
     * @param array $data Data to be emailed.
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Assembles the mail.
     *
     * @return $this The instance of this class, which Lumen will use to render and send the final email.
     * @since 1.0.0
     */
    public function build()
    {
        return $this->from(env('MAIL_FROM_ADDRESS'))
                    ->subject('A Visitor Contacted You, Guy Smiley')
                    ->view('emails.contact_form')
                    ->with([
                        'full_name' => $this->data['full_name'],
                        'email' => $this->data['email'],
                        'phone' => $this->data['phone'],
                        'guest_message' => nl2br($this->data['guest_message'])
                    ]);
    }
}
