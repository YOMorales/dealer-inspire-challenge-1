<div>
    <p>Hi Guy Smiley, now you have more reason to smile. A visitor contacted you. He left these details:</p>
    <p>Full Name: {{ $full_name }}</p>
    <p>Email: {{ $email }}</p>
    <p>Phone (if any): {{ $phone }}</p>
    <p>Message: {{ $guest_message }}</p>
</div>
