<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dealer Inspire Code Challenge</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">

    <!-- Theme CSS -->
    <link href="css/grayscale.min.css" rel="stylesheet">

    <!-- Custom CSS created by Y.O.Morales -->
    <link href="css/custom.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">
                    <i class="fa fa-play-circle"></i> <span class="light">Dealer</span> Inspire Challenge
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#about">About</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#coffee">Coffee</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#contact">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h1 class="brand-heading">Challenge</h1>
                        <p class="intro-text">Code Something Awesome.
                            <br>We <3 PHP Developers.</p>
                        <a href="#about" class="btn btn-circle page-scroll">
                            <i class="fa fa-angle-double-down animated"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- About Section -->
    <section id="about" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>About This Challenge</h2>
                <p>We make awesome things at Dealer Inspire.  We'd like you to join us.  That's why we made this page.  Are you ready to join the team?</p>
                <p>To take the code challenge, visit <a href="https://bitbucket.org/dealerinspire/php-contact-form">this Git Repo</a> to clone it and start your work.</p>
            </div>
        </div>
    </section>

    <section id="coffee" class="content-section text-center">
        <div class="download-section">
            <div class="container">
                <div class="col-lg-8 col-lg-offset-2">
                    <h2>Coffee Break?</h2>
                    <p>Take a coffee break.  You deserve it.</p>
                    <a href="https://www.youtube.com/dealerinspire" class="btn btn-default btn-lg">or Watch YouTube</a>
                </div>
            </div>
        </div>
    </section>

    <!-- Contact Section -->
    <section id="contact" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>Contact Guy Smiley</h2>
                <p>Remember Guy Smiley?  Yeah, he wants to hear from you.</p>

                <form action='/messages/add' method='post' id='form-contact' class='form-horizontal'>
                    <div class='form-group'>
                        <label for='field-full-name' class='col-sm-3 control-label'>Full Name</label>
                        <div class='col-sm-7'>
                            <div id='full-name-handler'>
                            <input name='Message[full_name]' id='field-full-name' placeholder='Your full name' required='required' minlength='6' maxlength='80' class='form-control'
                                data-parsley-trigger='change' data-parsley-class-handler='#full-name-handler'
                                data-parsley-errors-container='#full-name-error' data-parsley-error-message='Please, enter your full name.'>
                            </div>
                            <div id='full-name-error'></div>
                        </div>
                    </div>

                    <div class='form-group'>
                        <label for='field-email' class='col-sm-3 control-label'>Email</label>
                        <div class='col-sm-7'>
                            <div id='email-handler'>
                            <input name='Message[email]' type='email' id='field-email' placeholder='Your email address' required='required' maxlength='80' class='form-control'
                                data-parsley-type='email' data-parsley-trigger='change' data-parsley-class-handler='#email-handler'
                                data-parsley-errors-container='#email-error' data-parsley-error-message='Please, enter a valid, existing email address.'>
                            </div>
                            <div id='email-error'></div>
                        </div>
                    </div>

                    <div class='form-group'>
                        <label for='field-phone' class='col-sm-3 control-label'>Phone</label>
                        <div class='col-sm-7'>
                            <div id='phone-handler'>
                            <input name='Message[phone]' type='tel' id='field-phone' placeholder='(Optional) Your phone number' maxlength='20' class='form-control'
                                data-parsley-pattern='/^[0-9\-\(\) ]+$/i' data-parsley-trigger='change' data-parsley-class-handler='#phone-handler'
                                data-parsley-errors-container='#phone-error' data-parsley-error-message='Please, enter a valid phone number.'>
                            </div>
                            <div id='phone-error'></div>
                        </div>
                    </div>

                    <div class='form-group'>
                        <label for='field-guest-message' class='col-sm-3 control-label'>Message</label>
                        <div class='col-sm-7'>
                            <div id='guest-message-handler'>
                            <textarea name='Message[guest_message]' id='field-guest-message' required='required' minlength='10' maxlength='510' class='form-control' style='min-height: 100px;'
                                data-parsley-trigger='change' data-parsley-class-handler='#guest-message-handler'
                                data-parsley-errors-container='#guest-message-error' data-parsley-required-message='Please, Guy Smiley really wants to hear from you.'></textarea>
                            </div>
                            <div id='guest-message-error'></div>
                        </div>
                    </div>

                    <button type='submit' id='btn-submit-contact-form' class='btn btn-success'><i class='glyphicon glyphicon-check'></i> Submit</button>
                    <br>
                    <div id='general-error' style="margin-top: 12px;"></div>
                </form>

            </div>
        </div>
    </section>

    <!-- Map Section -->
    <div id="map"></div>

    <!-- Footer -->
    <footer>
        <div class="container text-center">
            <p><small>Copyright 2017 Dealer Inspire</small></p>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/ -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script>

    <!-- Theme JavaScript -->
    <script src="js/grayscale.min.js"></script>

    <!-- Parsley form validation plugin -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/parsley.js/2.3.15/parsley.min.js" integrity="sha256-mwzM5h8hG48mKAjubS2xy2iRYJOvutlOEJYOfB9i7jw=" crossorigin="anonymous"></script>

    <script type='text/javascript'>
        $(document).ready(function() {
            // instantiates form validation
            $('#form-contact').parsley();

            // posts form data via ajax so we can get a confirmation message and render it without page refresh
            // note: this pages uses jQuery version v1.12.4 (!)... would be good to upgrade
            $('#form-contact').submit(function(event) {
                event.preventDefault();
                // disables the submit button to prevent repeated clicks
                $('#btn-submit-contact-form').attr('disabled', 'disabled');
                // resets the general error display
                $('#general-error').text('');
                $('#general-error').removeClass();

                $.ajaxSetup({cache: false});
                $.post("/messages/add", $('#form-contact').serialize()).done(function(response) {
                    if (response == '{"status":"success"}') {
                        $('#general-error').text('Your message was processed and sent.');
                        $('#general-error').addClass('btn btn-success');
                    } else {
                        $('#general-error').text('An error happened and we could not process the contact form.');
                        $('#general-error').addClass('btn btn-danger');
                    }
                }).fail(function(response) {
                    $('#general-error').text('An error happened and we could not process the contact form.');
                    $('#general-error').addClass('btn btn-danger');
                }).always(function() {
                    $('#btn-submit-contact-form').removeAttr('disabled'); // restores the submit button
                });
            });

        });
    </script>

</body>

</html>
