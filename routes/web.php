<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// displays a landing/home page with basic information and a contact form
$router->get('/', function () {
    return view('home');
});

// URL that processes results of the contact form; accepts only POST
$router->post('/messages/add', [
    'uses' => 'MessagesController@add'
]);
