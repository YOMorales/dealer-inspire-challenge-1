<?php
/**
 * Test case for Messages Controller usage of the database.
 *
 * PHP version 7.1
 *
 * @package MessagesApp\Tests
 * @category Tests
 * @version 1.0.0
 *
 * @filesource
 *
 * @author Y.O.Morales <yomorales@gmail.com>
 * @copyright 2018 Dealer Inspire
 * @license http://dealerinspire.com Proprietary software.
 */

use Laravel\Lumen\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Mail;

/**
 * MessagesControllerDatabaseUsageTest
 *
 * @final
 * @since 1.0.0
 */
final class MessagesControllerDatabaseUsageTest extends TestCase
{

    use DatabaseMigrations;

    /**
     * The setUp() method is called before each test here.
     *
     * @since 1.0.0
     */
    public function setUp()
    {
        parent::setUp();

        // prevents any email from being sent
        Mail::fake();
    }

    /**
     * Test method.
     *
     * See method name and assertion messages for more information.
     *
     * @return void
     * @since 1.0.0
     */
    public function testSavingDataAvoidsSqlInjection()
    {
        $data = [
            'Message' => [
                'full_name' => 'Evil Test',
                'email' => 'test@example.com',
                'phone' => '',
                'guest_message' => "'); DROP TABLE messages; SELECT ('pawned"
            ]
        ];

        $response = $this->call('POST', '/messages/add', $data);

        $this->assertEquals('{"status":"success"}', $response->getContent(),
            "Field 'guest_message' with bad SQL should have been escaped and thus no fatal errors should have happened.");

        $this->seeInDatabase('messages', ['email' => 'test@example.com']);
    }

    /**
     * Test method.
     *
     * See method name and assertion messages for more information.
     *
     * @return void
     * @since 1.0.0
     */
    public function testSavingGoodDataIsSuccessful()
    {
        $data = [
            'Message' => [
                'full_name' => 'Good Test',
                'email' => 'goodtest@example.com',
                'phone' => '111-222-3333',
                'guest_message' => 'Testing saving good data to the database.'
            ]
        ];

        $response = $this->call('POST', '/messages/add', $data);

        $this->assertEquals('{"status":"success"}', $response->getContent(),
            "Saving good, valid data should have returned success.");

        $this->seeInDatabase('messages', ['email' => 'goodtest@example.com']);
    }

}
