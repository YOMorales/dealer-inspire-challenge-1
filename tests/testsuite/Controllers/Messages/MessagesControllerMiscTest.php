<?php
/**
 * Test case for miscellaneous code and functionality of Messages Controller.
 *
 * PHP version 7.1
 *
 * @package MessagesApp\Tests
 * @category Tests
 * @version 1.0.0
 *
 * @filesource
 *
 * @author Y.O.Morales <yomorales@gmail.com>
 * @copyright 2018 Dealer Inspire
 * @license http://dealerinspire.com Proprietary software.
 */

use Laravel\Lumen\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactFormMail;

/**
 * MessagesControllerMiscTest
 *
 * @final
 * @since 1.0.0
 */
final class MessagesControllerMiscTest extends TestCase
{

    use DatabaseMigrations;

    /**
     * Test method.
     *
     * See method name and assertion messages for more information.
     *
     * @return void
     * @since 1.0.0
     */
    public function testContactFormEmailIsSent()
    {
        $data = [
            'Message' => [
                'full_name' => 'Email Test',
                'email' => 'emailtest@example.com',
                'phone' => '111-222-3333',
                'guest_message' => 'Testing sending emails.'
            ]
        ];

        // let's mock the email sending functionality
        Mail::fake();

        $response = $this->call('POST', '/messages/add', $data);

        Mail::assertSent(ContactFormMail::class, function ($mail) {
            return $mail->hasTo(env('MAIL_TO_ADDRESS'));
        });
    }

}
