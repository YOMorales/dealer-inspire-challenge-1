<?php
/**
 * Test case for Messages Controller sanitation done in its actions.
 *
 * PHP version 7.1
 *
 * @package MessagesApp\Tests
 * @category Tests
 * @version 1.0.0
 *
 * @filesource
 *
 * @author Y.O.Morales <yomorales@gmail.com>
 * @copyright 2018 Dealer Inspire
 * @license http://dealerinspire.com Proprietary software.
 */

use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\MessagesController;

/**
 * MessagesControllerSanitationsTest
 *
 * @final
 * @since 1.0.0
 */
final class MessagesControllerSanitationsTest extends TestCase
{

    /**
     * @var object
     */
    private $MessagesController;

    /**
     * The setUp() method is called before each test here.
     * setUpBeforeClass() method was not used because that is static, and we wanted to use instantiation via $this.
     *
     * Also, testing the controller via direct instantiation instead of relying on calling routes.
     *
     * @since 1.0.0
     */
    public function setUp()
    {
        parent::setUp();

        $this->MessagesController = new MessagesController();

        // prevents any email from being sent
        Mail::fake();
    }

    /**
     * The tearDown() method is called after each test here.
     *
     * @since 1.0.0
     */
    public function tearDown()
    {
        parent::tearDown();

        $this->MessagesController = null;
    }

    /**
     * Test method.
     *
     * See method name and assertion messages for more information.
     *
     * @return void
     * @since 1.0.0
     */
    public function testRetainsOnlyWhitelistedFields()
    {
        $fields = [
            'full_name' => '',
            'email' => '',
            'guest_message' => '',
            'malicious_field' => ''
        ];

        $retained_fields = $this->MessagesController->retainWhitelistedFields($fields);

        $this->assertEquals(['full_name' => '', 'email' => '', 'guest_message' => '', 'phone' => ''], $retained_fields,
            "Method retainWhitelistedFields() should have returned an array with four valid fields, yet a different " .
                "array was given.");
    }

    /**
     * Test method.
     *
     * See method name and assertion messages for more information.
     *
     * @return void
     * @since 1.0.0
     */
    public function testInputSanitation()
    {
        $evil_data = [
            'full_name' => "Evil+%3Cscript+src%3D%27http%3A%2F%2Fexample.com%2Fevil.js%27%3E%3C%2Fscript%3E",
            'email' => 'test@example.com',
            'phone' => '',
            'guest_message' => "Malicious <script src='http://example.com/malicious.js'></script>"
        ];

        $clean_data = $this->MessagesController->sanitizeInput($evil_data);

        $this->assertEquals('Evil', $clean_data['full_name'],
            "Field 'full_name' should have been urldecoded, cleaned of tags, and trimmed, producing the string 'Evil'");

        $this->assertSame(null, $clean_data['phone'],
            "Field 'phone' was an empty string and thus should have been converted to null.");

        $this->assertEquals('Malicious', $clean_data['guest_message'],
            "Field 'guest_message' should have been cleaned of tags and trimmed, producing the string 'Malicious'");
    }

}
