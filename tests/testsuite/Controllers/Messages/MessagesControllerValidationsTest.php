<?php
/**
 * Test case for Messages Controller validations done in its actions.
 *
 * PHP version 7.1
 *
 * @package MessagesApp\Tests
 * @category Tests
 * @version 1.0.0
 *
 * @filesource
 *
 * @author Y.O.Morales <yomorales@gmail.com>
 * @copyright 2018 Dealer Inspire
 * @license http://dealerinspire.com Proprietary software.
 */

use Illuminate\Support\Facades\Mail;

/**
 * MessagesControllerValidationsTest
 *
 * @final
 * @since 1.0.0
 */
final class MessagesControllerValidationsTest extends TestCase
{

    /**
     * The setUp() method is called before each test here.
     *
     * @since 1.0.0
     */
    public function setUp()
    {
        parent::setUp();

        // prevents any email from being sent
        Mail::fake();
    }

    /**
     * Method that generates test data for the validations in other methods below.
     *
     * Note: PHPUnit data producers methods were not used because those require assertions that pass. Also PHPUnit's data
     * providers methods were not used because those require a specific array structure that is not the desired one.
     *
     * @return array An array with dummy data.
     * @final
     * @since 1.0.0
     * @todo Use the Faker library instead: https://github.com/fzaninotto/Faker
     */
    final private function generateTestData()
    {
        return [
            'Message' => [
                'full_name' => 'Mr PHPUnit Test',
                'email' => 'test@example.com',
                'phone' => '111-222-3333',
                'guest_message' => 'Lorem ipsum dolor sit amet.'
            ]
        ];
    }

    /**
     * Test method.
     *
     * See method name and assertion messages for more information.
     *
     * @return void
     * @since 1.0.0
     */
    public function testFullNameFailsWhenEmpty()
    {
        $data = $this->generateTestData();
        $data['Message']['full_name'] = '';

        $response = $this->call('POST', '/messages/add', $data);

        $expected = '{"Message.full_name":["The message.full name field is required."]}';
        $this->assertJsonStringEqualsJsonString($expected, $response->getContent(),
            "Empty 'full_name' field should have returned a specific error string.");
    }

    /**
     * Test method.
     *
     * See method name and assertion messages for more information.
     *
     * @return void
     * @since 1.0.0
     */
    public function testFullNameFailsWhenNotString()
    {
        $data = $this->generateTestData();
        $data['Message']['full_name'] = 3;

        $response = $this->call('POST', '/messages/add', $data);

        $expected = '{"Message.full_name":["The message.full name must be a string."]}';
        $this->assertJsonStringEqualsJsonString($expected, $response->getContent(),
            "Non-string 'full_name' field should have returned a specific error string.");
    }

    /**
     * Test method.
     *
     * See method name and assertion messages for more information.
     *
     * @return void
     * @since 1.0.0
     */
    public function testFullNameFailsWhenNotWithinLength()
    {
        // tests minimum length
        $data = $this->generateTestData();
        $data['Message']['full_name'] = str_random(2);
        $response = $this->call('POST', '/messages/add', $data);

        $expected = '{"Message.full_name":["The message.full name must be at least 6 characters."]}';
        $this->assertJsonStringEqualsJsonString($expected, $response->getContent(),
            "Field 'full_name' with less than 6 characters should have returned a specific error string.");

        // now tests maximum length
        $data['Message']['full_name'] = str_random(81);
        $response = $this->call('POST', '/messages/add', $data);

        $expected = '{"Message.full_name":["The message.full name may not be greater than 80 characters."]}';
        $this->assertJsonStringEqualsJsonString($expected, $response->getContent(),
            "Field 'full_name' with more than 80 characters should have returned a specific error string.");
    }

    /**
     * Test method.
     *
     * See method name and assertion messages for more information.
     *
     * @return void
     * @since 1.0.0
     */
    public function testEmailFailsWhenEmpty()
    {
        $data = $this->generateTestData();
        $data['Message']['email'] = '';

        $response = $this->call('POST', '/messages/add', $data);

        $expected = '{"Message.email":["The message.email field is required."]}';
        $this->assertJsonStringEqualsJsonString($expected, $response->getContent(),
            "Empty 'email' field should have returned a specific error string.");
    }

    /**
     * Test method.
     *
     * See method name and assertion messages for more information.
     *
     * @return void
     * @since 1.0.0
     */
    public function testEmailFailsWhenInvalidFormat()
    {
        $data = $this->generateTestData();
        $data['Message']['email'] = 'not-an-email';

        $response = $this->call('POST', '/messages/add', $data);

        $expected = '{"Message.email":["The message.email must be a valid email address."]}';
        $this->assertJsonStringEqualsJsonString($expected, $response->getContent(),
            "An 'email' field with an invalid email format should have returned a specific error string.");
    }

    /**
     * Test method.
     *
     * See method name and assertion messages for more information.
     *
     * @return void
     * @since 1.0.0
     */
    public function testEmailFailsWhenExceedsMaximumLength()
    {
        $data = $this->generateTestData();
        $data['Message']['email'] = str_random(81) . '@example.com';

        $response = $this->call('POST', '/messages/add', $data);

        // note: apparently Lumen doesn't obey well the 'max' validation rule with emails and produces this error anyway
        $expected = '{"Message.email":["The message.email must be a valid email address."]}';
        $this->assertJsonStringEqualsJsonString($expected, $response->getContent(),
            "Field 'email' with more than 80 characters should have returned a specific error string.");
    }

    /**
     * Test method.
     *
     * See method name and assertion messages for more information.
     *
     * @return void
     * @since 1.0.0
     */
    public function testPhoneFailsWhenDisobeyRegex()
    {
        $data = $this->generateTestData();
        $data['Message']['phone'] = '1a1 [8@Z] 777';

        $response = $this->call('POST', '/messages/add', $data);

        $expected = '{"Message.phone":["The message.phone format is invalid."]}';
        $this->assertJsonStringEqualsJsonString($expected, $response->getContent(),
            "Field 'phone' with characters other than these '0-9 -()' should have returned a specific error string.");
    }

    /**
     * Test method.
     *
     * See method name and assertion messages for more information.
     *
     * @return void
     * @since 1.0.0
     */
    public function testPhoneFailsWhenExceedsMaximumLength()
    {
        $data = $this->generateTestData();
        $data['Message']['phone'] = '123456780123456789012345';

        $response = $this->call('POST', '/messages/add', $data);

        $expected = '{"Message.phone":["The message.phone may not be greater than 20 characters."]}';
        $this->assertJsonStringEqualsJsonString($expected, $response->getContent(),
            "Field 'phone' with more than 20 characters should have returned a specific error string.");
    }

    /**
     * Test method.
     *
     * See method name and assertion messages for more information.
     *
     * @return void
     * @since 1.0.0
     */
    public function testMessageFailsWhenEmpty()
    {
        $data = $this->generateTestData();
        $data['Message']['guest_message'] = '';

        $response = $this->call('POST', '/messages/add', $data);

        $expected = '{"Message.guest_message":["The message.guest message field is required."]}';
        $this->assertJsonStringEqualsJsonString($expected, $response->getContent(),
            "Empty 'guest_message' field should have returned a specific error string.");
    }

    /**
     * Test method.
     *
     * See method name and assertion messages for more information.
     *
     * @return void
     * @since 1.0.0
     */
    public function testMessageFailsWhenNotString()
    {
        $data = $this->generateTestData();
        $data['Message']['guest_message'] = 123456789012345;

        $response = $this->call('POST', '/messages/add', $data);

        $expected = '{"Message.guest_message":["The message.guest message must be a string."]}';
        $this->assertJsonStringEqualsJsonString($expected, $response->getContent(),
            "Non-string 'guest_message' field should have returned a specific error string.");
    }

    /**
     * Test method.
     *
     * See method name and assertion messages for more information.
     *
     * @return void
     * @since 1.0.0
     */
    public function testMessageFailsWhenNotWithinLength()
    {
        // tests minimum length
        $data = $this->generateTestData();
        $data['Message']['guest_message'] = str_random(8);
        $response = $this->call('POST', '/messages/add', $data);

        $expected = '{"Message.guest_message":["The message.guest message must be at least 10 characters."]}';
        $this->assertJsonStringEqualsJsonString($expected, $response->getContent(),
            "Field 'guest_message' with less than 10 characters should have returned a specific error string.");

        // now tests maximum length
        $data['Message']['guest_message'] = str_random(511);
        $response = $this->call('POST', '/messages/add', $data);

        $expected = '{"Message.guest_message":["The message.guest message may not be greater than 510 characters."]}';
        $this->assertJsonStringEqualsJsonString($expected, $response->getContent(),
            "Field 'guest_message' with more than 510 characters should have returned a specific error string.");
    }

}
