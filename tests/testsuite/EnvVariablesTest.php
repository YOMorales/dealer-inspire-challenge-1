<?php
/**
 * Test case for 'MessagesApp' configuration variables.
 *
 * PHP version 7.1
 *
 * @package MessagesApp\Tests
 * @category Tests
 * @version 1.0.0
 *
 * @filesource
 *
 * @author Y.O.Morales <yomorales@gmail.com>
 * @copyright 2018 Dealer Inspire
 * @license http://dealerinspire.com Proprietary software.
 */

/**
 * EnvVariablesTest
 *
 * @final
 * @since 1.0.0
 */
final class EnvVariablesTest extends TestCase
{

    /**
     * Test method.
     *
     * See method name and assertion messages for more information.
     *
     * @return void
     * @since 1.0.0
     */
    public function testExecutingFromTestingEnvironment()
    {
        $environment = env('APP_ENV');

        $this->assertNotEmpty($environment, "APP_ENV evaluates as empty, when it shouldn't be.");
        $this->assertEquals('testing', $environment, "APP_ENV should have a value of 'testing'.");
    }

    /**
     * Test method.
     *
     * See method name and assertion messages for more information.
     *
     * @return void
     * @since 1.0.0
     */
    public function testOtherRequiredVarsArePresent()
    {
        $this->assertInternalType('string', env('APP_KEY'),
            "APP_KEY should be a string, but a different variable type was given.");

        $this->assertInternalType('string', env('DB_DATABASE'),
            "DB_DATABASE should be a string, but a different variable type was given.");

        $this->assertInternalType('string', env('DB_CHARSET'),
            "DB_CHARSET should be a string, but a different variable type was given.");

        $this->assertInternalType('string', env('DB_COLLATION'),
            "DB_COLLATION should be a string, but a different variable type was given.");
    }

}
