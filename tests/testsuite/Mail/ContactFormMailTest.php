<?php
/**
 * Test case for the ContactFormMail class.
 *
 * PHP version 7.1
 *
 * @package MessagesApp\Tests
 * @category Tests
 * @version 1.0.0
 *
 * @filesource
 *
 * @author Y.O.Morales <yomorales@gmail.com>
 * @copyright 2018 Dealer Inspire
 * @license http://dealerinspire.com Proprietary software.
 */

use App\Mail\ContactFormMail;

/**
 * ContactFormMailTest
 *
 * @final
 * @since 1.0.0
 */
final class ContactFormMailTest extends TestCase
{

    /**
     * Test method.
     *
     * See method name and assertion messages for more information.
     *
     * @return void
     * @since 1.0.0
     */
    public function testClassPropertiesAreSetCorrectly()
    {
        $data = [
            'full_name' => 'Good Test',
            'email' => 'goodtest@example.com',
            'phone' => '111-222-3333',
            'guest_message' => 'Lorem ipsum dolor sit amet.'
        ];

        // instantiates the class corresponding to the contact form mail
        $mail = new ContactFormMail($data);
        $mail = $mail->build();

        $this->assertEquals('messagesmailer@example.com', $mail->from[0]['address'],
            "The email's 'from' address is different from what is expected.");

        $this->assertEquals('A Visitor Contacted You, Guy Smiley', $mail->subject,
            "The email's subject is different from what is expected.");

        $this->assertEquals($data, $mail->viewData,
            "The email's view variables are different from what is expected.");
    }

}
