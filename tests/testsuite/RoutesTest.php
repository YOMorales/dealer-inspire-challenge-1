<?php
/**
 * Test case for 'MessagesApp' defined routes.
 *
 * PHP version 7.1
 *
 * @package MessagesApp\Tests
 * @category Tests
 * @version 1.0.0
 *
 * @filesource
 *
 * @author Y.O.Morales <yomorales@gmail.com>
 * @copyright 2018 Dealer Inspire
 * @license http://dealerinspire.com Proprietary software.
 */

/**
 * RoutesTest
 *
 * Tried to test that route definitions match (but do not call) controller actions, but this proved to be
 * difficult and time-consuming with Lumen.
 *
 * @final
 * @since 1.0.0
 */
final class RoutesTest extends TestCase
{

    /**
     * Test method.
     *
     * See method name and assertion messages for more information.
     *
     * @return void
     * @since 1.0.0
     */
    public function testRootUrlRendersHome()
    {
        $response = $this->call('GET', '/');

        $this->assertSame(200, $response->status(),
            "Status code for home should be integer 200 yet a different status was given.");

        $this->assertEquals('home', $response->original->getName(),
            "Root url should have used 'home' view yet a different one was used.");
    }

    /**
     * Test method.
     *
     * See method name and assertion messages for more information.
     *
     * @return void
     * @since 1.0.0
     */
    public function testRouteMessagesaddOnlyAcceptsPost()
    {
        // 'testing negatively' in order to not actually call the route using POST
        $unallowed_methods = ['GET', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'];
        foreach ($unallowed_methods as $http_method) {
            $response = $this->call($http_method, '/messages/add');

            $this->assertSame(405, $response->status(),
                "Sending $http_method request to 'messages/add' route should have given 405 ('Method Not Allowed') " .
                    "yet a different status was given.");
        }
    }

    /**
     * Test method.
     *
     * See method name and assertion messages for more information.
     *
     * @return void
     * @since 1.0.0
     */
    public function testNonExistentRouteGives404()
    {
        $response = $this->call('GET', '/nonexistent');

        $this->assertSame(404, $response->status(),
            "Status code for nonexistent route should be integer 404 yet a different status was given.");
    }

}
